package com.example.piyush.touchlistener.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.piyush.touchlistener.R;


public class MainActivity extends ActionBarActivity {
    RelativeLayout layout;
    TextView status;
    Button button, nextButton;
    Drawable drawableRed, drawableGreen;
    int centerX, centerY, radius;
    boolean stateRed = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layout = (RelativeLayout) findViewById(R.id.layout);
        status = (TextView) findViewById(R.id.status);
        button = (Button) findViewById(R.id.button);
        nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MultipleBallsActivity.class);
                startActivity(intent);
            }
        });
        drawableRed = getResources().getDrawable(R.drawable.round_button_red);
        drawableGreen = getResources().getDrawable(R.drawable.round_button_green);
        button.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                radius = (button.getRight() - button.getLeft()) / 2;
                centerX = button.getLeft() + radius;
                centerY = button.getTop() + radius;
            }
        });
        layout.setOnTouchListener(new CustomTouchListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class CustomTouchListener implements View.OnTouchListener {

        Double squareX, squareY, distance;
        String text = "";
        Boolean flag = true;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    squareX = Math.pow((event.getX() - centerX), 2);
                    squareY = Math.pow((event.getY() - centerY), 2);
                    distance = Math.sqrt(squareX + squareY);
                    if (distance == radius) {
                        text = "ON BOUNDARY";
                    } else if (distance < radius) {
                        text = "INSIDE";
                        if (flag) {
                            stateRed = !stateRed;
                            button.setBackground(stateRed ? drawableRed : drawableGreen);
                            flag = false;
                        }
                    } else {
                        flag = true;
                        text = "OUTSIDE";
                    }
                    status.setText(text);
                    break;
                case MotionEvent.ACTION_UP:
                    flag = true;
                    status.setText("TOUCH REMOVED");
                    break;
                case MotionEvent.ACTION_MOVE:
                    squareX = Math.pow((event.getX() - centerX), 2);
                    squareY = Math.pow((event.getY() - centerY), 2);
                    distance = Math.sqrt(squareX + squareY);
                    if (distance == radius) {
                        text = "ON BOUNDARY";
                    } else if (distance < radius) {
                        text = "INSIDE";
                        if (flag) {
                            stateRed = !stateRed;
                            button.setBackground(stateRed ? drawableRed : drawableGreen);
                            flag = false;
                        }
                    } else {
                        flag = true;
                        text = "OUTSIDE";
                    }
                    status.setText(text);
                    break;
            }
            return true;
        }
    }
}
