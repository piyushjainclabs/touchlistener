package com.example.piyush.touchlistener.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.piyush.touchlistener.R;


public class MultipleBallsActivity extends ActionBarActivity {
    int[][] center;
    Button[] buttons;
    Button submit;
    EditText editRows, editColumns;
    RelativeLayout layout;
    int layoutWidth, layoutHeight, row, col;
    int leftMargin, topMargin, leftMarginToSet = 0, topMarginToSet = 0;
    int c = 0;
    Drawable drawableRed, drawableGreen;
    Double[][] squaredDistances;
    Double radius = 30.0;
    Boolean[] flag, stateRed;
    TextView status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_balls);
        layout = (RelativeLayout) findViewById(R.id.layout_multiple);
        editRows = (EditText) findViewById(R.id.rows);
        editColumns = (EditText) findViewById(R.id.columns);
        status = (TextView) findViewById(R.id.status);
        drawableRed = getResources().getDrawable(R.drawable.round_button_red);
        drawableGreen = getResources().getDrawable(R.drawable.round_button_green);
        layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layoutWidth = layout.getMeasuredWidth();
                layoutHeight = layout.getMeasuredHeight();

            }
        });

        submit = (Button) findViewById(R.id.submit);

        layout.post(new Runnable() {
            @Override
            public void run() {
                do {

                } while (layoutWidth == 0);

                submit.setVisibility(View.VISIBLE);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        row = Integer.parseInt(editRows.getText().toString().trim());
                        col = Integer.parseInt(editColumns.getText().toString().trim());
                        leftMargin = (layoutWidth - (col * 60)) / (col + 1);
                        topMargin = (layoutHeight - (row * 60)) / (row + 1);
                        buttons = new Button[row * col];
                        center = new int[row * col][2];
                        squaredDistances = new Double[row * col][3];
                        flag = new Boolean[row * col];
                        stateRed = new Boolean[row * col];

                        for (int k = 0; k < (row * col); k++) {
                            stateRed[k] = true;
                        }

                        topMarginToSet = topMargin;
                        for (int i = 1; i <= row; i++) {
                            leftMarginToSet = leftMargin;
                            for (int j = 1; j <= col; j++) {
                                buttons[c] = new Button(MultipleBallsActivity.this);
                                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(60, 60);
                                params.setMargins(leftMarginToSet, topMarginToSet, 0, 0);
                                center[c][0] = leftMarginToSet + 30;
                                center[c][1] = topMarginToSet + 30;
                                buttons[c].setLayoutParams(params);
                                buttons[c].setPadding(0, 0, 0, 0);
                                buttons[c].setBackgroundResource(R.drawable.round_button_red);
                                buttons[c].setClickable(false);
                                buttons[c].setFocusable(false);
                                buttons[c].setFocusableInTouchMode(false);
                                layout.addView(buttons[c]);
                                c++;
                                leftMarginToSet += (leftMargin + 60);
                            }
                            topMarginToSet += (topMargin + 60);
                        }
                    }
                });
            }
        });
        layout.setOnTouchListener(new CustomTouchListener(c));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_multiple_balls, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class CustomTouchListener implements View.OnTouchListener {
        String text = "";

        CustomTouchListener(int c) {
            for (int i = 0; i < c; i++) {
                flag[i] = true;
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    for (int i = 0; i < c; i++) {
                        squaredDistances[i][0] = Math.pow((event.getX() - center[i][0]), 2);
                        squaredDistances[i][1] = Math.pow((event.getY() - center[i][1]), 2);
                        squaredDistances[i][2] = Math.sqrt(squaredDistances[i][0] + squaredDistances[i][1]);
                        if (squaredDistances[i][2] == radius) {
                            text = "ON BOUNDARY";
                        } else if (squaredDistances[i][2] < radius) {
                            text = "INSIDE Circle" + (i + 1);
                            if (flag[i]) {
                                stateRed[i] = !stateRed[i];
                                buttons[i].setBackground(stateRed[i] ? drawableRed : drawableGreen);
                                flag[i] = false;
                            }
                            break;
                        } else {
                            flag[i] = true;
                            text = "OUTSIDE";
                        }
                    }
                    status.setText(text);
                    break;
                case MotionEvent.ACTION_UP:
                    for (int i = 0; i < c; i++)
                        flag[i] = true;
                    status.setText("TOUCH REMOVED");
                    break;
                case MotionEvent.ACTION_MOVE:
                    for (int i = 0; i < c; i++) {
                        squaredDistances[i][0] = Math.pow((event.getX() - center[i][0]), 2);
                        squaredDistances[i][1] = Math.pow((event.getY() - center[i][1]), 2);
                        squaredDistances[i][2] = Math.sqrt(squaredDistances[i][0] + squaredDistances[i][1]);
                        if (squaredDistances[i][2] == radius) {
                            text = "ON BOUNDARY";
                        } else if (squaredDistances[i][2] < radius) {
                            text = "INSIDE Circle" + (i + 1);
                            if (flag[i]) {
                                stateRed[i] = !stateRed[i];
                                buttons[i].setBackground(stateRed[i] ? drawableRed : drawableGreen);
                                flag[i] = false;
                            }
                            break;
                        } else {
                            flag[i] = true;
                            text = "OUTSIDE";
                        }
                    }
                    status.setText(text);
                    break;
            }
            return true;
        }
    }
}
